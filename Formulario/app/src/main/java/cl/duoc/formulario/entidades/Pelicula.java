package cl.duoc.formulario.entidades;

import java.util.Date;

/**
 * Created by TanTania on 07-04-2017.
 */

public class Pelicula {
    private String nombre;
    private String director;
    private String categoria;
    private String fecha;
    private String urlImagen;

    public Pelicula(String nombre, String categoria, String director, String fecha, String urlImagen) {
        this.nombre = nombre;
        this.categoria = categoria;
        this.director = director;
        this.fecha = fecha;
        this.urlImagen = urlImagen;
    }

    public Pelicula() {

    }

    public String getUrlImagen() {
        return urlImagen;
    }

    public void setUrlImagen(String urlImagen) {
        this.urlImagen = urlImagen;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDirector() {
        return director;
    }

    public void setDirector(String director) {
        this.director = director;
    }

    public String getCategoria() {
        return categoria;
    }

    public void setCategoria(String categoria) {
        this.categoria = categoria;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }
}
