package cl.duoc.formulario;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Toast;

import java.util.Calendar;

import cl.duoc.formulario.bd.Almacen;
import cl.duoc.formulario.entidades.Pelicula;

public class FormularioActivity extends AppCompatActivity  implements View.OnClickListener{
     private EditText etNombre, etDire,etUrl;
     private Button btnAgregar, btnListar, etFecha;
    private CheckBox cCat1, cCat2,cCat3;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_formulario);

        etNombre= (EditText) findViewById(R.id.etNombre);
        etDire= (EditText) findViewById(R.id.etDire);
        etUrl= (EditText) findViewById(R.id.etUrl);

        btnListar= (Button)findViewById(R.id.btnListar);
        btnListar.setOnClickListener(this);

        btnAgregar= (Button)findViewById(R.id.btnAgregar);
        btnAgregar.setOnClickListener(this);

        cCat1=(CheckBox)findViewById(R.id.cCat1);
        cCat1.setOnClickListener(this);
        cCat2=(CheckBox)findViewById(R.id.cCAt2);
        cCat2.setOnClickListener(this);
        cCat3=(CheckBox)findViewById(R.id.cCat3);
        cCat3.setOnClickListener(this);

        etFecha =(Button)findViewById(R.id.etFecha);
        etFecha.setOnClickListener(this);




    }




    private DatePickerDialog getDialog() {
        Calendar c = Calendar.getInstance();
        int anio = c.get(Calendar.YEAR);
        int mes = c.get(Calendar.MONTH);
        int dia = c.get(Calendar.DAY_OF_MONTH);


        return new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                etFecha.setText(year + "/" + formatDate((month + 1)) + "/" + formatDate(dayOfMonth));
            }
        }, anio, mes, dia);

    }

    private String formatDate(int value) {

        return (value > 9 ? value + "" : "0" + value);

    }

    @Override
    public void onClick(View v) {

        if (v.getId() == R.id.etFecha) {
            getDialog().show();
        }
        if (v.getId() == R.id.btnListar) {
            Intent i = new Intent(FormularioActivity.this, ListadoActivity.class);
            startActivity(i);
        } else if (v.getId() == R.id.btnAgregar) {
            String mensajeError = "";
            String cat = "";

            if (etNombre.getText().toString().length() < 1) {
                mensajeError += "Ingrese Nombre \n";
            }
            if (etDire.getText().toString().length() < 1) {
                mensajeError += "Ingrese Director \n";
            }
            if (!cCat1.isChecked()&& !cCat2.isChecked()&& !cCat3.isChecked() ) {
                mensajeError += "Ingrese Categoria \n";}
            if (etFecha.getText().toString().length() < 1) {
                mensajeError += "Ingrese Fecha \n";
            }
            if (etUrl.getText().toString().length() < 1) {
                mensajeError += "Ingrese url \n";
            }

            if(mensajeError.length()>0){
                Toast.makeText(this, mensajeError, Toast.LENGTH_SHORT).show();
            }
            else {
                Pelicula peli = new Pelicula();
                peli.setNombre(etNombre.getText().toString());
                peli.setDirector(etDire.getText().toString());
                peli.setUrlImagen(etUrl.getText().toString());

                if (cCat1.isChecked()) {

                    cat+= "Romance ";

                }
                if (cCat2.isChecked()) {
                    cat+= " Misterio ";

                }
                if (cCat3.isChecked()) {
                    cat+= " Comedia ";
                }
                peli.setCategoria(cat);
                peli.setFecha(etFecha.getText().toString());
                Almacen.agregarFormulario(peli);
                Toast.makeText(this, " Guardada correctamente ! Nombre : " + peli.getNombre() +" Categorias: " + peli.getCategoria()+" URL: " + peli.getUrlImagen()  , Toast.LENGTH_LONG).show();



            }
        }
    }}
