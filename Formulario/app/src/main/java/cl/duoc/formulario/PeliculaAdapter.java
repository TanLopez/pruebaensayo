package cl.duoc.formulario;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import cl.duoc.formulario.bd.Almacen;
import cl.duoc.formulario.entidades.Pelicula;

/**
 * Created by DUOC on 08-04-2017.
 */

public class PeliculaAdapter extends ArrayAdapter<Pelicula> {

    private ArrayList<Pelicula> dataSource;

    public PeliculaAdapter(Context context, ArrayList<Pelicula> dataSource){
        super(context, R.layout.activity_listado, dataSource);
        this.dataSource = dataSource;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = LayoutInflater.from(getContext());
        View item = inflater.inflate(R.layout.activity_listado, null);

        TextView tvNombre = (TextView)item.findViewById(R.id.tvNombre);
        tvNombre.setText(dataSource.get(position).getNombre());

        TextView tvCategoria = (TextView)item.findViewById(R.id.tvCategoria);
        tvCategoria.setText(dataSource.get(position).getCategoria());

        TextView tvEstreno = (TextView)item.findViewById(R.id.tvEstreno);
        tvEstreno.setText(dataSource.get(position).getFecha());

        ImageView ivPreview= (ImageView)item.findViewById(R.id.ivPreview);
        Picasso.with(getContext()).load(dataSource.get(position).getUrlImagen()).into(ivPreview);


        return(item);
    }


}
